"use strict";

import * as crypto from 'crypto';
import download from 'download';
import * as fs from "fs";
import * as net from 'net'
import * as path from "path";
import { commands, workspace, ExtensionContext, window, ProgressLocation } from "vscode";
import { ChildProcess, spawn } from "child_process";

import {
    LanguageClient,
    LanguageClientOptions,
    StreamInfo,
    Trace,
} from "vscode-languageclient";

function generateRandomServerPort(): number {
    const minPort = 10000;
    const maxPort = 40000;

    return Math.floor((Math.random() * (maxPort - minPort)) + minPort);
}

// function delay(ms: number): Promise<NodeJS.Timer> {
//     return new Promise( (resolve: (value?: (NodeJS.Timer)) => void) => setTimeout(resolve, ms) );
// }

let client: LanguageClient;
let serverProcess: ChildProcess;

const distributionJobNumber = "735379565";
const fileChecksum = "1c7f0e49d48e91ee4a27b0d817a8f78d1bfa8221";

export function activate(context: ExtensionContext) {
    const port = generateRandomServerPort();
    const serverExecutable = path.join(__dirname, '/distribution.phar');

    const serverOptions = () =>
        new Promise<ChildProcess | StreamInfo>(async (resolve, reject) => {

            const serverExists = fs.existsSync(serverExecutable);
            const mayDownload = config.get<boolean>('disableServerDownload') !== true;

            if (mayDownload && (!serverExists || !(await checkFile(serverExecutable, fileChecksum)))) {
                await downloadAndCheck(
                    `https://gitlab.com/Serenata/Serenata/-/jobs/${distributionJobNumber}/artifacts/raw/bin/distribution.phar`,
                    serverExecutable,
                    fileChecksum
                );
            } else if (!mayDownload) {
                if (!serverExists) {
                    throw new Error(
                        `No server executable exists in ${serverExecutable} and downloading it was disabled by ` +
                        'setting "serenata.disableServerDownload" to true. Either manually create or symlink the ' +
                        'executable or reenable downloading.'
                    );
                } else {
                    console.warn(
                        'Serenata server executable is not up to date according to hash, but it will not be updated ' +
                        'automatically because "serenata.disableServerDownload" is set to "true". This may be ' +
                        'intentional if you have symlinked or created this file manually for development purposes.'
                    );
                }
            }

            serverProcess = spawn('php', [
                // TODO: Make configurable.
                "-dmemory_limit=2000M",
                serverExecutable,
                '--uri=tcp://127.0.0.1:' + port
            ]);

            if (serverProcess.stdout === null || serverProcess.stderr === null) {
                throw new Error('Server process was spawned but did not have STDOUT or STDERR pipe due to unknown error');
            }

            serverProcess.stdout.on('data', (data: Buffer) => {
                const message = data.toString();

                console.debug('The PHP server has something to say:', message);

                if (message.indexOf('Starting server bound') !== -1) {
                    // Assume the server has successfully spawned the moment it says it's listening.
                    const clientConnection = net.createConnection(port);

                    clientConnection.on('data', (chunk: Buffer) => {
                        const str = chunk.toString();
                        console.log(str);
                    });

                    resolve({ reader: clientConnection, writer: clientConnection })
                }
            });

            serverProcess.stderr.on('data', (data: Buffer) => {
                console.error('The PHP server has errors to report:', data.toString());
            });

            serverProcess.on('close', (code) => {
                if (code === 2) {
                    console.error(`Port ${port} is already taken`);
                } else if (code !== 0 && code !== null) {
                    const detail =
                        'Serenata unexpectedly closed. Either something caused the process to stop, it crashed, ' +
                        'or the socket closed. In case of the first two, you should see additional output ' +
                        'indicating this is the case and you can report a bug. If there is no additional output, ' +
                        'you may be missing the right dependencies or extensions or the server may have run out ' +
                        'of memory (you can increase it via the settings screen).';

                    console.error(detail);
                }

                reject();
            });
        });

    const config = workspace.getConfiguration('serenata');

    let clientOptions: LanguageClientOptions = {
        // Register the server for plain text documents
        documentSelector: [
            {
                pattern: "**/*.php",
                scheme: "file"
            },
        ],
        synchronize: {
            configurationSection: "serenata",
            fileEvents: workspace.createFileSystemWatcher("**/*.php")
        },
        initializationOptions: {
            configuration: config.get<object>('configuration'),
        }
    };

    // Create the language client and start the client.
    client = new LanguageClient(
        "serenata",
        "Serenata",
        serverOptions,
        clientOptions
    );

    client.clientOptions.errorHandler;
    client.trace = Trace.Verbose;
    let disposable = client.start();

    // The server requests to execute this command on the server, so just forward it.
    commands.registerCommand('serenata/command/openTextDocument', () => {
        // TODO: Seem to be missing the data to pass to the command. We need to forward this to the server and it is
        // part of the spec.
        console.log("Code lens command triggered", arguments);

        // client.sendRequest('workspace/executeCommand', {
        //     command: 'serenata/command/openTextDocument', // Same as one we got, just forward.
        //     arguments: arguments,
        // });
    });

    // In response to commands such as openTextDocument, the server might request to open a text document.
    client.onRequest('serenata/openTextDocument', (parameters) => {
        console.log("Received request to open text document", parameters);

        /*
            TODO: Should be roughly equivalent of

            atom.workspace.open(Convert.uriToPath(parameters.uri), {
                initialLine: parameters.position.line,
                searchAllPanes: true,
            });
        */
    });

    // Push the disposable to the context's subscriptions so that the
    // client can be deactivated on extension deactivation
    context.subscriptions.push(disposable);
}

export function deactivate(): Thenable<void> {
    if (!client) {
        return new Promise((resolve) => {
            resolve();
        });
    }

    return client.stop().then(() => {
        serverProcess.kill();
    });
}

function downloadAndCheck(url: string, dest: string, hash: string) {
    return window.withProgress(
        {
            location: ProgressLocation.Notification,
            title: "Downloading PHP Language Server",
            cancellable: false,
        },
        (progress, _) =>
            download(url, path.dirname(dest), {
            filename: path.basename(dest),
            }).on("response", (res) => {

                function formatBytes(bytes: number, decimals = 2) {
                    if (bytes === 0) return '0 Bytes';
                
                    const k = 1024;
                    const dm = decimals < 0 ? 0 : decimals;
                    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                
                    const i = Math.floor(Math.log(bytes) / Math.log(k));
                
                    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
                }

                // Progress does not work with GitLab, as it does not provide "content-length"
                const total: number | undefined = res.headers['content-length'];
                let current = 0;
        
                res.on("data", (data: Buffer) => {
                    current += data.length;

                    let increment: number | undefined;
                    let message = formatBytes(current);

                    if (total != null) {
                        increment = (100 * data.length) / total
                        message += ` / ${formatBytes(total)}`;
                    }
                    
                    progress.report({
                        increment,
                        message
                    });
                });
            }).then(() => {
                if (!checkFile(dest, hash)) {
                    throw new Error("Invalid file downloaded");
                }
            })
        );
  }

function checkFile(path: string, hash: string) {
    return new Promise<boolean>((resolve, reject) => {
        fs.readFile(path, function(err, data) {
            if (err) {
                reject(err)

            } else {
                var checksum = crypto
                    .createHash("sha1");
                
                checksum.write(data, "utf8");

                const digest = checksum.digest("hex");

                resolve(digest === hash);
            }
        });
    });
}
