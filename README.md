<p align="center">
<img src="https://serenata.gitlab.io/images/logo.png" alt="Serenata logo"</img>
</p>

# Visual Studio Code Extension for Serenata
Integrates [Serenata](https://serenata.gitlab.io/) into Visual Studio Code, providing autocompletion, code navigation, refactoring, signature help, linting and code lenses for PHP.

This package should also work with _Code (OSS)_ and [VSCodium](https://vscodium.com/).

See also [the website](https://serenata.gitlab.io/) on [how to set it up](https://serenata.gitlab.io/#what-do-i-need) and more screenshots.

## This Package Needs a Maintainer!
The base for the integration was provided generously by [Oliver Nybroe](https://gitlab.com/olivernybroe). Some additional enhancements have been done by [Gert-dev](https://gitlab.com/Gert-dev) and [Rodrigo Tavares](https://gitlab.com/rodrigost23) to improve the integration.

This package is in a working state, but is still somewhat rough around the edges. It should be considered a working prototype.

The following improvements could be made to this base:

* It is not possible to configure the memory limit that is passed to the server.
* Serenata sends the `serenata/openTextDocument` request when clicking a code lens. There is some commented out code to handle this, but it doesn't work yet. All this needs to do is open the received file URI and scroll to the specified cursor position.
* This package sometimes also appears to activate itself for files that do not contain PHP code or projects that don't contain any PHP code at all.
* Publish it on the [Visual Studio Code Marketplace](https://marketplace.visualstudio.com) and/or the [Open VSX Registry](https://open-vsx.org/) for public consumption.

## Installation
This package is not yet available on the official extension store, since it is not in a complete state. You have two choices to install it currently: use the VSIX or build from source.

### VSIX
1. Go to the [pipelines](https://gitlab.com/Serenata/visual-studio-code-client/-/pipelines) page.
2. Click the download button of the most recent passed pipeline and download the `build:typescript:archive` artifact.
3. Extract the archive.
4. [Open Visual Studio Code and install the VSIX via the extensions page](https://code.visualstudio.com/docs/editor/extension-gallery#_install-from-a-vsix).
5. Go to [the folder of the extension](https://code.visualstudio.com/docs/editor/extension-gallery#_where-are-extensions-installed) and place `distribution.phar` (the Serenata server binary) inside the `out` folder.

### Build From Source

#### npm (Recommended)
Run the following commands in the cloned repository folder:

```
npm install
npm run build
```

#### [pnpm](https://pnpm.js.org)
Run the following commands in the cloned repository folder:

```
pnpm install
pnpm run build
```

`pnpm` appears to work fine for development, but may be problematic during packaging with `vsce` as `vsce` hardcodes npm and packaging the `node_modules` folder happens improperly because they contain symlinks.

#### Activate
The package is now built. You can create a symlink in `~/.vscode/extensions` (or `~/.vscode-oss/extensions`, depending on what variant you are running) to the cloned repository folder and the extension should be loaded after restarting the editor.

## Project Configuration
This extension exposes some settings that you can modify. Using Visual Studio Code's functionality to configure settings per project or folder, you get per-project Serenata configuration for free.

For example, in `.vscode/settings.json`:

```json
{
    "serenata.configuration": {
        "uris": [
            "file:///home/user/my-project"
        ],
        "indexDatabaseUri": "file:///home/me/.cache/vscode-serenata/my-project.sqlite",
        "phpVersion": 7.3,
        "excludedPathExpressions": [],
        "fileExtensions": [
            "php"
        ]
    }
}

```

## Disable Server Downloading
Set `serenata.disableServerDownload` to `true` to stop the package from downloading the server. This can be handy if you're developing the server itself and have symlinked it - otherwise the package will redownload the server because the checksum does not match.

![AGPLv3 Logo](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)
